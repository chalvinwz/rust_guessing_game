FROM rust:1.64-alpine

COPY . .

RUN cargo build --release

ENTRYPOINT ["./target/release/rust_guessing_game"]