use std::io;
use std::cmp::Ordering;
use rand::{thread_rng, Rng};

fn main() {
    let secret_number = thread_rng().gen_range(1..=100);
    
    loop {
        println!("==================== Guess The Number ====================");

        println!("Input your guess:");

        let mut guess: String = String::new();
        
        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line!");
        
        println!("\nYou guessed {guess}");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_)   => {
                println!("You should input a valid number!");
                println!("==========================================================\n");
                continue;
            },
        };

        match guess.cmp(&secret_number) {
            Ordering::Less => {
                println!("Too small, try again!");
                println!("==========================================================\n");
            },
            Ordering::Greater => {
                println!("Too big, try again!");
                println!("==========================================================\n");
            },
            Ordering::Equal => {
                println!("You win!");
                println!("=================== Thanks for playing ===================");
                break;
            },
        }
    }
}
