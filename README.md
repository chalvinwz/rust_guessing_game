# Rust Tutorial

This is one of tutorial program from [The Rust Programming Language](https://doc.rust-lang.org/book/ch02-00-guessing-game-tutorial.html) Book.

## How to play this game

### Using docker

```sh
docker run -it --name rust_guessing_game chalvinwz/rust_guessing_game
```

### Using cargo

- Make sure you have rustup configured or you can it install first [here](https://www.rust-lang.org/tools/install). This will install all the necessary things for you including cargo.

- Clone this repository (_you know how_).

- Jump into the directory and run this command:

```sh
  cargo run
```

### Thank you!
